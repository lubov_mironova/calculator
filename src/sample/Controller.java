package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

@SuppressWarnings("unused")
public class Controller {

    @FXML
    private TextField firstNumberField;

    @FXML
    private TextField secondNumberField;

    @FXML
    private TextField resultField;

    @FXML
    private Label errorLabel;

    public static final String NOT_A_NUMBER = "Введите числа";
    public static final String EMPTY_FIELD = "Пустое поле";
    public static final String ERROR = "На ноль делить нельзя!";

    /**
     * Проверяет первое и второе числовое поле на наличие введенных данных
     *
     * @return 0, если хотя бы одно поле пустое и 1, если в оба поля явведены данные
     */
    private int checkForEmpty() {
        if (firstNumberField.getText().isEmpty() || secondNumberField.getText().isEmpty()) {
            errorLabel.setText(EMPTY_FIELD);
            return 0;
        }
        return 1;
    }

    /**
     * Сохраняет введенные в текстовые поля значения
     *
     * @param field текст из текстового поля
     * @return число или null, если введены нечисловые данные
     */
    private Double getText(TextField field) {
        try {
            return Double.parseDouble(field.getText());
        } catch (Exception e) {
            errorLabel.setText(NOT_A_NUMBER);
        }
        return null;
    }

    /**
     * Очищает метку с ошибкой
     */
    private void clearErrorLabel() {
        errorLabel.setText("");
    }

    @FXML
    void clear(ActionEvent event) {
        firstNumberField.clear();
        secondNumberField.clear();
        resultField.clear();
        clearErrorLabel();
    }

    @FXML
    void div(ActionEvent event) {
        if (checkForEmpty() == 1) {
            if (getText(firstNumberField) != null && getText(secondNumberField) != null) {
                double first = getText(firstNumberField);
                double second = getText(secondNumberField);
                if (second != 0) {
                    double div = first / second;
                    clearErrorLabel();
                    resultField.setText(String.valueOf(div));
                } else {
                    errorLabel.setText(ERROR);
                }
            }
        }
    }

    @FXML
    void mul(ActionEvent event) {
        if (checkForEmpty() == 1) {
            if (getText(firstNumberField) != null && getText(secondNumberField) != null) {
                double first = getText(firstNumberField);
                double second = getText(secondNumberField);
                double mul = first * second;
                clearErrorLabel();
                resultField.setText(String.valueOf(mul));
            }
        }
    }

    @FXML
    void sub(ActionEvent event) {
        if (checkForEmpty() == 1) {
            if (getText(firstNumberField) != null && getText(secondNumberField) != null) {
                double first = getText(firstNumberField);
                double second = getText(secondNumberField);
                double sub = first - second;
                clearErrorLabel();
                resultField.setText(String.valueOf(sub));
            }
        }
    }

    @FXML
    void sum(ActionEvent event) {
        if (checkForEmpty() == 1) {
            if (getText(firstNumberField) != null && getText(secondNumberField) != null) {
                double first = getText(firstNumberField);
                double second = getText(secondNumberField);
                double sum = first + second;
                clearErrorLabel();
                resultField.setText(String.valueOf(sum));
            }
        }
    }
}